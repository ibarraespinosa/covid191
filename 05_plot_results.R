options(encoding = "UTF-8")
Sys.setlocale("LC_TIME", "English")
library(data.table, quietly = TRUE, warn.conflicts = FALSE)
library(mgcv)
library(ggplot2)


ff5 <- readRDS("rds/ff4.rds")
ff5$Temperature <- ff5$TEMP
ff5$RH <- ff5$UR
sapply(ff5, class)
dateini <- as.Date("2020-03-27")
dateend <-  Sys.Date()#as.Date("2020-09-21")#Sys.Date()#

ff5 <- ff5[date >= dateini & date <dateend]


ff5$casosNovos <- as.numeric(ff5$casosNovos)
ff5$obitosNovos <- as.numeric(ff5$obitosNovos)

ff5[, month := month(date)]
ff5$id <- 1:nrow(ff5)
ff5[, dow2 := wday(x = date)] # Sunday is 1,Saturday is 7
ff5$dia <- strftime(ff5$date, "%a")
ff5$dow <- c(ff5$dow2[1] -1, ff5$dow2[1:(length(ff5$dow2) - 1) ])




# data in long format ####
names(ff5)
ffmSPcasos <- ff5[, c("date", "O3", "PM25", "Temperature", "RH",
                      "RMI_mSP",
                      "McasosNovos",
                      "id", "dow")]
ffmSPcasos$type = "Cases"
ffmSPobitos <- ff5[, c("date", "O3", "PM25", "Temperature", "RH",
                       "RMI_mSP",
                       "MobitosNovos",
                       "id", "dow")]
ffmSPobitos$type = "Deaths"

ffSP <- rbind(ffmSPcasos, ffmSPobitos, use.names = F)
ffSP$region = "muni"
names(ffSP)[6:7] <- c("RMI", "COVID19")

head(ffSP)
ffSP <- ffSP[!is.na(RMI)]

DF1 <- readRDS("RDS/DF_google.rds")
DF2 <- readRDS("RDS/DF.rds")
DF <- rbind(DF1, DF2)
setDT(DF)
DF$alpha <- ifelse(DF$ppv_rmi < 0.05, 1, 0.2)
DF$dist <- factor(DF$dist, levels = unique(DF$dist))
DF$mobi <- factor(DF$mobi, levels = unique(DF$mobi)[2:1])

fn <- function(x) image_write(image_trim(image_read(x)), x)

# merging figures
p1 <- ggplot(DF[ma >3], 
             aes(x = mconf, 
                 y = coef_RMI, 
                 colour = dist, 
                 alpha = alpha,
                 shape = dist)) + 
  geom_point(size = 2) +
  geom_errorbar(aes(ymin = coef_RMI_inf, 
                    ymax = coef_RMI_sup),
                size = 0.5) +
  scale_color_manual(values = c("blue", "red")) +
  geom_hline(yintercept = 0) +
  theme_clean() +
  facet_grid(mobi+type~ma)+
  scale_alpha(guide = 'none')+
  scale_x_continuous(breaks = c(0, 10))+
  labs(y = expression(beta[1]), x = "models")+
  theme_clean() +
  theme(panel.spacing.x=unit(0, "lines"),
        panel.spacing.y=unit(0, "lines"),
        legend.position = 'bottom',
        legend.title = element_blank(),
        legend.text=element_text(size=18),
        axis.title.y = element_text(size = 14),
        axis.text.y  = element_text(size = 14),
        strip.text.x = element_text(size = 12),
        strip.text.y = element_text(size = 14))

png(filename = "figs/beta1g.png",
    width = 3500, height = 4000, units = "px", res = 300)
print(p1)
dev.off()
fn("figs/beta1g.png")




x <- DF[ma >3 & mobi == "RMI SIMI-SP" & ppv_rmi<0.05 & ppv_in<0.05]
x
summary(ff5$RMI_mSP)

lapply(1:nrow(x), function(i){
  df <- data.frame(COVID19 = exp(x$coef_inter[i]+x$coef_RMI[i]*37:60),
                   RMI = 37:60)
  df$COVID19_inf <- exp(x$coef_inter_inf[i]-x$coef_RMI_inf[i]*37:60)
  df$COVID19_sup <-  exp(x$coef_inter_sup[i]-x$coef_RMI_sup[i]*37:60)
  df
}) -> dl
length(37:60)

dl <- rbindlist(dl)
dl$model <- rep(x$model, each = 24)
dl$conf <- rep(x$conf, each = 24)
dl$mconf <- rep(x$mconf, each = 24)
dl$ma <- rep(x$ma, each = 24)
dl$type <- rep(x$type, each = 24)
dl$dist <- rep(x$dist, each = 24)
dl$alpha <- ifelse(dl$model == "Average", 1, 0.2)


# cases ####
cases <- dl[model != "Average" & type == "Cases"]
ob <- gam(formula = COVID19~s(RMI, bs= "tp"),
          data = cases)
summary(ob)
confint.default(ob)

pred <- predict(ob, cases, se=TRUE)
cases$pred <- pred$fit
cases$lwl <- pred$fit-1.96*pred$se.fit
cases$upl <- pred$fit+1.96*pred$se.fit

plot(x = cases$RMI, y = cases$pred)
points(x = cases$RMI, y = cases$lwl)
points(x = cases$RMI, y = cases$upl)

# deaths ####
deaths <- dl[model != "Average" & type == "Deaths"]
ob <- gam(formula = COVID19~s(RMI, bs= "tp"),
          data = deaths)
summary(ob)
confint.default(ob)

pred <- predict(ob, deaths, se=TRUE)
deaths$pred <- pred$fit
deaths$lwl <- pred$fit-1.96*pred$se.fit
deaths$upl <- pred$fit+1.96*pred$se.fit

plot(x = deaths$RMI, y = deaths$pred)
points(x = deaths$RMI, y = deaths$lwl)
points(x = deaths$RMI, y = deaths$upl)

cases
DF <- rbind(cases, deaths)

summary(ffSP$RMI)

p3 <- ggplot(DF[model != "Average"], 
             aes(x = RMI, 
                 y = COVID19)) + 
  geom_vline(xintercept = qmed, lty = 2, size = 1)+
  geom_vline(xintercept = q1, lty = 2, size = 1)+
  geom_vline(xintercept = q3, lty = 2, size = 1)+
  geom_point(size = 2, pch = 16, col = "#808080") +
  geom_smooth(col = "red")+
  geom_line(aes(y = lwl), color = "blue")+ 
  geom_line(aes(y = upl), color = "blue")+ 
  theme_clean() +
  facet_wrap(~type, scales = "free", ncol = 2)+
  scale_alpha(guide = 'none')+
  labs(y = "COVID-19", x = "RMI SIMI-SP")+
  theme_clean() +
  theme(panel.spacing.x=unit(0, "lines"),
        panel.spacing.y=unit(0, "lines"),
        legend.position = 'bottom',
        legend.title = element_blank(),
        legend.text=element_text(size=18),
        axis.title.y = element_text(size = 14),
        axis.text.x = element_text(size = 14),
        axis.text.y  = element_text(size = 14),
        strip.text.x = element_text(size = 14),
        strip.text.y = element_text(size = 14))

p3

png(filename = "figs/COVID19.png",
    width = 3500, height = 2000, units = "px", res = 300)
print(p3)
dev.off()

qmin <- min(ffSP$RMI, na.rm = T)
q1 <- quantile(ffSP$RMI, .25)
qmed <- median(ffSP$RMI)
q3 <- quantile(ffSP$RMI, .75)
qmax <- max(ffSP$RMI)

round(unique(DF[RMI == round(qmed), c("pred", "lwl", "upl")]))
round(unique(DF[RMI == round(q1), c("pred", "lwl", "upl")]))
round(unique(DF[RMI == round(q3), c("pred", "lwl", "upl")]))
round(unique(DF[RMI == round(qmin), c("pred", "lwl", "upl")]))
round(unique(DF[RMI == 50, c("pred", "lwl", "upl")]))
round(unique(DF[RMI == round(qmax), c("pred", "lwl", "upl")]))


p4 <- ggplot(data = ffSP,
             aes(x = RMI)) +
  geom_vline(xintercept = qmed, lty = 2, size = 1)+
  geom_vline(xintercept = q1, lty = 2, size = 1)+
  geom_vline(xintercept = q3, lty = 2, size = 1)+
  geom_histogram(col= "black", fill = "transparent", size = 1)+
  theme_clean() +
  facet_wrap(~type, scales = "free", ncol = 2)+
  scale_alpha(guide = 'none')+
  labs(y = "Histogram RMI SIMI-SP\n", x = NULL)+
  theme_clean() +
  theme(panel.spacing.x=unit(1, "lines"),
        panel.spacing.y=unit(0, "lines"),
        legend.title = element_blank(),
        legend.text=element_text(size=18),
        axis.title.y = element_text(size = 14),
        axis.text.x = element_blank(),
        axis.text.y  = element_text(size = 14),
        strip.text.x = element_text(size = 14, colour = "white"),
        strip.text.y = element_text(size = 14, colour = "white"),
        rect = element_rect(fill = "white"))

p4

png(filename = "figs/COVID19den.png",
    width = 3500, height = 700, units = "px", res = 300)
print(p4)
dev.off()
fn("figs/COVID19den.png")
fn("figs/COVID19.png")

i1 <- image_read("figs/COVID19.png")
i2 <- image_read("figs/COVID19den.png")
image_write(image_append(c(i2, i1), stack = T),
            "figs/COVID19_final.png")


# pollutant ####

DF <- readRDS("rds/DF_POL.rds")
setDT(DF)
DF$alpha <- ifelse(DF$ppv_POL < 0.05, 1, 0.2)

DF$dist <- factor(DF$dist, levels = unique(DF$dist))

DF$labs <- ifelse(
  DF$pol == "PM2.5", paste0("PM[2.5]"),
  ifelse(
    DF$pol == "O3", paste0("O[3]"),
    ifelse(
      DF$pol == "UR",paste0("Relative~Humidity~", "('%')"),
      #teclado en portuges
      # alt dereito +E
      paste0("Temperature~(","'°C')"))))

p5 <- ggplot(DF, 
             aes(x = mconf, 
                 y = exp(coef_POL*10), 
                 colour = dist, 
                 alpha = alpha,
                 shape = dist)) + 
  geom_point(size = 2) +
  geom_errorbar(aes(ymin = exp(coef_POL_inf*10), 
                    ymax = exp(coef_POL_sup*10)),
                size = 0.5) +
  scale_color_manual(values = c("blue", "red")) +
  geom_hline(yintercept = 1) +
  theme_clean() +
  facet_grid(labs+type~ma, labeller = label_parsed)+
  
  scale_alpha(guide = 'none')+
  scale_x_continuous(breaks = c(0, 4, 8))+
  labs(y = expression(exp(beta[1]%*%10)), x = "models")+
  theme_clean() +
  theme(panel.spacing.x=unit(0, "lines"),
        panel.spacing.y=unit(0, "lines"),
        legend.position = 'bottom',
        legend.title = element_blank(),
        legend.text=element_text(size=18),
        axis.title.y = element_text(size = 14),
        axis.text.y  = element_text(size = 14),
        strip.text.x = element_text(size = 12),
        strip.text.y = element_text(size = 14))

png(filename = "figs/betapol.png",
    width = 3500, height = 3000, units = "px", res = 300)
print(p5)
dev.off()
fn <- function(x) image_write(image_trim(image_read(x)), x)
fn("figs/betapol.png")



x <- DF[ppv_POL <0.05 & coef_POL > 0, 
        lapply(.SD, mean), 
        .SDcols = c("coef_POL", "coef_POL_inf", "coef_POL_sup"), 
        by = .(type, ma, pol)]
x$RR <- round(exp(x$coef_POL*10), 3)
x$RRinf <- round(exp(x$coef_POL_sup*10), 3)
x$RRsup <- round(exp(x$coef_POL_inf*10), 3)
x

x[pol == "O3", c("type", "ma", "RR", "RRinf", "RRsup")]
xx <- x[pol == "O3", c("type", "ma", "RR", "RRinf", "RRsup")]
setorderv(xx, "ma")
xx[type == "Cases"]
xx[type == "Deaths"]

x[pol == "PM2.5", c("type", "ma", "RR", "RRinf", "RRsup")]
xx <- x[pol == "PM2.5", c("type", "ma", "RR", "RRinf", "RRsup")]
setorderv(xx, "ma")
xx[type == "Cases"]
xx[type == "Deaths"]

yy <- x[, 
        lapply(.SD, mean),
        .SDcols=c("RR", "RRinf", "RRsup"),
        by = .(type, pol)]
yy$RR <- round(yy$RR, 3)
yy$RRinf <- round(yy$RRinf, 3)
yy$RRsup <- round(yy$RRsup, 3)
yy
